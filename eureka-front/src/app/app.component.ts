import { Component } from '@angular/core';
import {SearchCepService} from './services/search-cep.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'eureka-front';

  public zipcode: any;
  public showAddress: any;
  public visibleAddress: any;


  constructor(private searchCepService: SearchCepService) {
  }

  clean_results(){
    this.visibleAddress = null;
    this.showAddress = false;
    this.zipcode = null;
  }
  search_cep(){

    if (this.zipcode.length != 8){
      alert('Por favor, informe um CEP válido.');
      return;
    }

    this.searchCepService.service_search_cep(this.zipcode).subscribe(data => {
      console.log(data);
      this.showAddress = true;
      this.visibleAddress = data['street'] + ' - ' + data['neighborhood'] + ' - ' + data['city'] + '/' + data['state'] + ' - ' + data['zipcode'];
    }, error => {
      alert('Houve um erro ao consultar o CEP. Verifique se o CEP está válido e tente novamente.');
    });

  }

}


