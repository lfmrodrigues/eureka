from django.test import TestCase
from rest_framework.test import APIClient

from eureka_back.settings import redis_server


class SearchCEPTest(TestCase):
    url = 'http://localhost:8000'
    client = APIClient()

    def test_search_cep_no_hifen(self):

        for key in redis_server.keys(pattern='*'):
            redis_server.delete(key)

        response = self.client.get(self.url + '/api/v1/search-cep/?zipcode=31070020')
        self.assertEqual(response.status_code, 200)

        if 'zipcode' in response.data:
            self.assertEqual(True, True)

    def test_search_cep_hifen(self):

        for key in redis_server.keys(pattern='*'):
            redis_server.delete(key)

        response = self.client.get(self.url + '/api/v1/search-cep/?zipcode=31070-020')
        self.assertEqual(response.status_code, 200)

        if 'zipcode' in response.data:
            self.assertEqual(True, True)
        else:
            self.assertEqual(True, False)

    def test_search_cep_wrong_cep_9_digits(self):

        for key in redis_server.keys(pattern='*'):
            redis_server.delete(key)

        response = self.client.get(self.url + '/api/v1/search-cep/?zipcode=111111111')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['errorCode'], 1)


    def test_search_cep_wrong_cep(self):

        for key in redis_server.keys(pattern='*'):
            redis_server.delete(key)

        response = self.client.get(self.url + '/api/v1/search-cep/?zipcode=11111111')
        self.assertEqual(response.status_code, 400)
