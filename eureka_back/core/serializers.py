from rest_framework import serializers


class BaseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)
