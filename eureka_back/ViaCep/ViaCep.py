import requests


class ViaCep(object):

    def get_address(self, cep):

        request = requests.get('http://viacep.com.br/ws/{}/json/'.format(cep))

        try:
            return request.status_code, request.json()
        except:
            return request.status_code, None
