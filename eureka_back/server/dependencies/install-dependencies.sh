#!/usr/bin/env bash

sudo apt-get install build-essential
sudo apt-get install binutils libproj-dev gdal-bin
sudo apt-get install libgeos++
sudo apt-get install proj-bin
sudo apt install gdal-bin
sudo apt install redis-server
sudo apt-get install libapache2-mod-rpaf
sudo apt-get install apache2
sudo apt-get install -y apache2-dev
sudo apt-get install libapache2-mod-wsgi-py3
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot python-certbot-apache
sudo certbot --apache
sudo certbot renew --dry-run

sudo apt-get install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib


# Inserir no CRONTAB
# certbot renew && /etc/init.d/apache2 restart


