from django.db import models


class Address(models.Model):
    zipcode = models.CharField(max_length=50)
    street = models.CharField(max_length=2048, blank=True, null=True)
    complement = models.CharField(max_length=50, blank=True, null=True)
    neighborhood = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=50, blank=True, null=True)
    ddd = models.CharField(max_length=50, blank=True, null=True)
