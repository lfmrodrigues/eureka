"""eureka_back URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from api_web.endpoints.SearchCep import SearchCEPAPIView

schema_view = get_schema_view(
    openapi.Info(
        title="Documentação API eureka_back",
        default_version='v1',
        description='''

        Esta documentação tem o objetivo de descrever os *endpoints* da API Eureka BACK.<br><br>
        
        Nas requisições que ocorram erros de validação, são retornados códigos de erro que descrevem um evento específico.
        Este código de erro, é único para toda a plataforma.<br>
        Os códigos de erros são apresentados dentro da documentação do *endpoint* específico. O JSON abaixo, mostra um exemplo de código de erro.

        ```json
        http_status_code = 400
        {
            "errorCode": 1
        }
        ```
        Caso ocorra um erro inesperado, como uma exceção, não é retornado um código de erro específico.
        No entanto, é aconselhável possuir um tratamento de erro genérico, caso receba uma resposta com código 400 ou 500.
        <br><br>
        Esta API tem um único endpoint /search-cep. Este endpoint é capaz de receber um CEP e pesquisar no WebService do ViaCep, o endereço associado.
        Foi implementado um mecanismo de cache baseado em memória, através do Redis. Assim, para cada consulta de CEP realizada com sucesso, os dados são armazenados no banco da aplicação PostgresSQL, além de ser armazenado o CEP em cache, 
        Por fim, para cada requisição de CEP, primeiramente, é consultado em cache se o CEP existe. Se sim, os dados são armazenados em banco. Se não, os dados são consultados no WebService do ViaCEP.<br><br>
        
        Por fins de simplicidade, este endpoint não possui autenticação.


        ''',
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('api/v1/search-cep/', SearchCEPAPIView.as_view()),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
