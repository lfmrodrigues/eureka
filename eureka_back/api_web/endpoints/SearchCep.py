from rest_framework.response import Response
from rest_framework.views import APIView
from ViaCep.ViaCep import ViaCep
from api_web.serializers.AddressSerializer import AddressSerializer
from domain.entity.Address import Address
from eureka_back.settings import redis_server


class SearchCEPAPIView(APIView):

    def get(self, request):
        """
        Realiza a consulta do endereço pelo cep informado na requisição. O cep deve ser informado por parâmetro
        querystring, exemplo: &zipcode=31070020

        ### Códigos de Erros
            * **errorCode = 1:** CEP com formáto inválido

        """

        zipcode = self.request.query_params.get('zipcode', None)

        if zipcode and len(zipcode) > 0:

            zipcode = zipcode.replace('-', '')

            if len(zipcode) != 8:
                return Response({'errorCode': 1}, status=400)

            # Verifica se o número do CEP está armazenado em Cache. Se sim, é feita a consulta no banco e a resposta
            # é concluída, se não, é consultado no serviço ViaCEP.
            if len(redis_server.keys(pattern=zipcode)) > 0:
                address = Address.objects.filter(zipcode=zipcode).first()
                return Response(AddressSerializer(address).data, status=200)

            else:
                address = ViaCep().get_address(cep=zipcode)
                if not address[1] or ('erro' in address[1] and address[1]['erro']):
                    return Response(status=400)

                else:
                    data = dict()
                    data['zipcode'] = address[1]['cep'].replace('-', '')
                    data['street'] = address[1]['logradouro']
                    data['complement'] = address[1]['complemento']
                    data['neighborhood'] = address[1]['bairro']
                    data['state'] = address[1]['uf']
                    data['ddd'] = address[1]['ddd']
                    data['city'] = address[1]['localidade']

                    serializer = AddressSerializer(data=data)
                    serializer.is_valid(raise_exception=True)
                    serializer.save()
                    redis_server.set(zipcode, '1')
                    return Response(serializer.data)

        else:
            return Response(status=404)
