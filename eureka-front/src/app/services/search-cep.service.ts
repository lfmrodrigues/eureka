import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SearchCepService {

  constructor(
    private http: HttpClient,
  ) { }
  service_search_cep(zipcode: string): Observable<any>{

    const headers = new HttpHeaders()
      .set('content-type', 'application/json');

    return this.http.get<any>('http://localhost:57930/api/v1/search-cep/?zipcode=' + zipcode, { 'headers': headers });
  }
}
