from core.serializers import BaseSerializer
from domain.entity.Address import Address
from rest_framework import serializers


class AddressSerializer(BaseSerializer):
    zipcode = serializers.CharField(help_text='CEP', allow_blank=True, allow_null=True)
    street = serializers.CharField(help_text='Logradouro', allow_blank=True, allow_null=True)
    complement = serializers.CharField(help_text='Complemento', allow_blank=True, allow_null=True)
    neighborhood = serializers.CharField(help_text='Bairro', allow_blank=True, allow_null=True)
    city = serializers.CharField(help_text='Cidade', allow_blank=True, allow_null=True)
    state = serializers.CharField(help_text='state', allow_blank=True, allow_null=True)
    ddd = serializers.CharField(help_text='DDD da Região', allow_blank=True, allow_null=True)

    class Meta:
        model = Address
        fields = ['id', 'zipcode', 'street', 'complement', 'neighborhood', 'city', 'state', 'ddd']

    def create(self, validated_data):
        address = Address.objects.create(**validated_data)
        return address
