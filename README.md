Este projeto tem a finalidade de implementar o teste requerido no processo de seleção da Eureka Labs. 

Este projeto, foi desenvolvido baseado em REST API. O Backend foi desenvolvido em Python/Django, enquanto o Frontend foi desenvolvido em Angular. 

No backend foi utilizado dois tipos de bancos: Redis e PostgreSQL.
A finalidade do banco Redis, é prover um mecanismo de cache baseado em memória. Assim, para uma requisição de CEP, este é verificado se está alocado no banco Redis (em memória). Se estiver alocado, sabe-se que este endereço está no banco de dados da aplicação, o PostgreSQL. Então os dados de endereço do CEP é consultado no banco e retornado para o usuário.
Caso o CEP não esteja no banco Redis, os dados do endereço são consultados no webservice ViaCEP, armazenado no banco da aplicação e também em cache. 
Esta arquitetura foi utilizada, pois consultas em bancos Redis são muito mais rápidas que os bancos tradicionais, como o caso do PostgreSQL, pois estes dados estão persistidos em memória. Caso uma consulta seja feita no banco da aplicação, com possibilidade de haver ou não o CEP, é que ao menos 1 consulta é feita neste banco para cada requisição. Variáveis como latência e concorrência de acesso ao banco, pode aumentar o tempo de espera do usuário, o que pode ser prejudicial para a experiência do usuário e para a aplicação como um todo. 

O Front end, implementa o endpoint para buscar endereços por CEP. 

Todos os módulos deste projetos estão disponibilizados em contâiners Docker. Para facilitar a integração entre os módulos e o build, criei um docker-compose na raiz deste projeto para disponibilizar todos os serviços. No entanto, temos os seguintes serviços, cada um sendo fornecidos separadamente: 

- Aplicação Front End (service front)
- Aplicação Banck End (service web)
- Banco Redis (service redis)
- Banco PostgreSQL (service db)

As portas de acesso às aplicações, configurei portas altas para diminuir a chance de haver conflito na máquina de quem vai avaliar este projeto. 
Portanto, para o Backend, foi utilizada a porta 57930 e para o Frontend, a porta 57931.

Foram desenvolvidos 4 testes unitários no Backend. Por fins de simplicidade, estes testes são executados sempre ao executar o container web. Portanto, nos logs, irá exibir : "Ran 4 tests in X seconds OK ".

Foi disponibilizada uma documentação da API em Swagger. Esta documentação pode ser conferida pelo link
http://localhost:57930/docs/

Por fim, caso no computador do avaliador não tenha o docker instalado. Disponibilizei um Shell Script na raíz deste projeto para instalação do mesmo. 


Para executar o projeto, localizado na pasta raíz, execute o comando: docker-compose up
